import { models } from "feathers-vuex"

const auth = {

  user: null,

  getUser () {
    return this.user
  },

  login (email, password) {
    return models.api.dispatch("auth/authenticate", {
      strategy: "local",
      email: email,
      password: password
    }).then((response) => {
      console.log("login successful")

      this.user = response.user

      return Promise.resolve(response.user)
    }).catch((err) => {
      console.log("login failed", err)

      this.user = null

      return Promise.reject(err)
    })
  },

  logout () {
    console.log("logout()")

    return models.api.dispatch("auth/logout")
      .then(() => {
        console.log("logout successful")

        this.user = null
        this.accessToken = null
      })
      .catch((err) => {
        console.log("logout failed", err)

        return Promise.reject(err)
      })
  }

}

export default auth
