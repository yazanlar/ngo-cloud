
const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "/home", name: "home", component: () => import("pages/Home") },
      { path: "/signin", name: "signin", component: () => import("pages/SignIn") }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  })
}

export default routes
