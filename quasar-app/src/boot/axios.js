import Vue from "vue"
import axios from "axios"

// old:
// Vue.prototype.$axios = axios

// https://stackoverflow.com/a/61039754/13260590
if (process.env.MODE === "electron") {
  var axiosInstance = axios.create({
    baseURL: process.env.API_URL
  })
  Vue.prototype.$axios = axiosInstance
} else {
  Vue.prototype.$axios = axios
}

// Here we define a named export
// that we can later use inside .js files:
export { axiosInstance }
